/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sheridan;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Admin
 */
public class ShoppingCartDemoTest {
    
    public ShoppingCartDemoTest() {
    }

    @org.junit.BeforeClass
    public static void setUpClass() throws Exception {
    }

    @org.junit.AfterClass
    public static void tearDownClass() throws Exception {
    }

    @org.junit.Before
    public void setUp() throws Exception {
    }

    @org.junit.After
    public void tearDown() throws Exception {
    }
    

    /**
     * Test of main method, of class ShoppingCartDemo.
     */
    @org.junit.Test
    public void testMain() {
        System.out.println("main");
        String[] args = null;
        ShoppingCartDemo.main(args);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }
 @Test
    public void GoodTest()
    {
        System.out.println("Testing Regular: ");
        String product = "Shirt";
        ShoppingCartDemo instance = new ShoppingCartDemo();
        boolean expResult = true;
        boolean result = ShoppingCartDemo.validateProduct(product);
        assertEquals(expResult, result);
        fail("Not an item");
    }
    @Test
    public void BadTest()
    {
        System.out.println("Testing Regular: ");
        String product = "L@ptops";
        ShoppingCartDemo instance = new ShoppingCartDemo();
        boolean expResult = true;
        assertEquals("Bad Expression, Please select a valid product",expResult);
    }   
}
